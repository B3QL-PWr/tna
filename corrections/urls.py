from django.conf.urls import url
from .views import CreateChangeRequest

urlpatterns = [
    url(r'^(?P<date>\d{4}-\d{2}-\d{2})$', CreateChangeRequest.as_view(), name='correct')
]
