from collections import defaultdict
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.db.models import Count, ExpressionWrapper, BooleanField, Exists, OuterRef
from django.db.models.functions import TruncDate
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django_extensions.management.jobs import DailyJob
from corrections.models import ChangeRequest

User = get_user_model()


class Job(DailyJob):
    help = "Send notifications about missing events."

    def execute(self):
        change_request = (
            ChangeRequest.objects
                .filter(requester=OuterRef('pk'), time__date=OuterRef('event_date'))
        )

        users = (
            User.objects
            .filter(supervisor__isnull=False, events__isnull=False)
            .annotate(event_date=TruncDate('events__created_at'))
            .values_list('email', 'event_date')
            .annotate(missing_event=ExpressionWrapper(Count('event_date') % 2, BooleanField()),
                      has_change_request=Exists(change_request, BooleanField()))
            .filter(missing_event=True, has_change_request=False, event_date__lt=timezone.now().date())
            .order_by('-event_date')
        )

        user_dates = defaultdict(list)
        for user_email, missing_event_date, _missing_event_flag, _change_request_flag in users:
            user_dates[user_email].append(missing_event_date)

        context = {'domain': get_current_site(request=None).domain}
        for email, dates in user_dates.items():
            context['dates'] = dates
            message = render_to_string('corrections/missing_events_notification_email.txt', context)
            send_mail('Missing card events', message, from_email=None, recipient_list=[email])