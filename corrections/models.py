from django.db import models
from django.contrib.auth import get_user_model
from config.utils import format_timedelta

class ChangeRequest(models.Model):
    requester = models.ForeignKey(get_user_model(), related_name='corrections')
    time = models.DateTimeField()

    def approve(self):
        self.requester.events.create(created_at=self.time)
        self.delete()

    def decline(self):
        self.delete()

    @property
    def worked_hours(self):
        last_event = self.requester.events.last(self.time)
        return format_timedelta(self.time - last_event.created_at)

    def __str__(self):
        return str(self.requester)