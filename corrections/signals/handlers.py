from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site

from ..models import ChangeRequest


@receiver(post_save, sender=ChangeRequest)
def send_change_request_notification(sender, instance, created, raw, using, update_fields, **kwargs):
    if created and not raw and instance.requester.supervisor:
        user = instance.requester
        context = {
            'domain': get_current_site(request=None).domain,
            'object': instance,
            'last_event': user.events.last(instance.time).created_at
        }
        message = render_to_string('corrections/supervisor_notification_email.txt', context=context)
        user.supervisor.email_user('New time change request', message, fail_silently=True)
