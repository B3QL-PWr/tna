from django.contrib import admin
from django.utils.formats import localize
from .models import ChangeRequest



class ChangeRequestAdmin(admin.ModelAdmin):
    list_display = ('requester', 'last_event_time', 'time', 'worked_hours')
    readonly_fields = ('requester', 'last_event_time', 'time', 'worked_hours')

    def last_event_time(self, obj):
        return localize(obj.requester.events.last(obj.time).created_at)

    def has_module_permission(self, request):
        return request.user.is_staff

    def has_add_permission(self, request):
        return False

    def save_model(self, request, obj, form, change):
        obj.approve()

    def delete_model(self, request, obj):
        obj.decline()

    def get_queryset(self, request):
        qs = super(ChangeRequestAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(requester__supervisor=request.user)
        return qs