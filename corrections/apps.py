from django.apps import AppConfig


class CorrectionsConfig(AppConfig):
    name = 'corrections'

    def ready(self):
        from .signals import handlers
