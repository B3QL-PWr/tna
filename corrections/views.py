from datetime import datetime
from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.utils.timezone import make_aware
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.contrib.messages.views import SuccessMessageMixin
from .forms import AddChangeRequestForm
from .models import ChangeRequest


class CreateChangeRequest(LoginRequiredMixin, SuccessMessageMixin, FormView):
    template_name = 'corrections/changerequest_form.html'
    form_class = AddChangeRequestForm
    success_message = 'Time saved successfully.'

    def _get_last_event(self):
        user = self.request.user
        events = user.events.filter(created_at__date=self.kwargs['date'])
        last_event = events.last()

        if len(events) % 2 == 0 or user.corrections.filter(time__date=last_event.created_at.date()).exists():
            return None

        return last_event

    def dispatch(self, request, *args, **kwargs):
        try:
            self.kwargs['date'] = datetime.strptime(self.kwargs['date'], '%Y-%m-%d').date()
        except:
            raise Http404()

        last_event = self._get_last_event()
        if last_event is None:
            raise PermissionDenied()

        self.initial.update({'last_event_time': last_event.created_at.time()})
        return super(CreateChangeRequest, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        date = self.kwargs['date'].isoformat()
        self.success_url = reverse_lazy('dashboard_day', kwargs={'date': date})
        return super(CreateChangeRequest, self).get_success_url()

    def form_valid(self, form):
        date = self.kwargs['date']
        time = form.cleaned_data['time']
        request_time = datetime(date.year, date.month, date.day, time.hour, time.minute, time.second)
        ChangeRequest(requester=self.request.user, time=make_aware(request_time)).save()
        return super(CreateChangeRequest, self).form_valid(form)