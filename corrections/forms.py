from django import forms
from bootstrap3_datetime.widgets import DateTimePicker


class AddChangeRequestForm(forms.Form):
    time = forms.TimeField(widget=DateTimePicker(icon_attrs= {'class': 'glyphicon glyphicon-time'},
                                                 options={'format': 'HH:mm'}))

    def clean_time(self):
        event_time = self.initial['last_event_time']
        if  event_time >= self.cleaned_data['time']:
            msg = 'Provided time should be between {:02d}:{:02d}:{:02d} and 23:59:59'
            raise forms.ValidationError(msg.format(event_time.hour, event_time.minute, event_time.second))
        return self.cleaned_data['time']
