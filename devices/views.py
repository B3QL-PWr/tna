from collections import defaultdict, Counter
from datetime import datetime, timedelta
from binascii import hexlify, unhexlify
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.views import View
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from nacl.public import Box, PublicKey
from nacl.encoding import HexEncoder
from config.utils import format_timedelta
from .models import Card, Device, Event


@method_decorator(csrf_exempt, name='dispatch')
class EventView(View):

    @method_decorator(login_required)
    def get(self, request):
        start_date, start_errors = self._parse_date_parameter(request, 'start')
        end_date, end_errors = self._parse_date_parameter(request, 'end')

        errors = [start_errors, end_errors]

        if any(errors):
            result =  dict(errors=list(filter(None, errors)))
            return JsonResponse(status=400, data=result)

        user_events = request.user.events.between(start_date, end_date)

        all_dates = Counter(event.created_at.date() for event in user_events)

        working_hours = defaultdict(timedelta)
        for start, end in user_events.working_ranges():
            working_hours[start.date()] += end - start

        calendar_events = []
        current_date = timezone.localdate()
        for date, events_num in all_dates.items():
            event = {'start': date.isoformat(),
                     'url': reverse_lazy('dashboard_day', args=(date.isoformat(), )),
                     'title': 'N/A'}

            if date in working_hours:
                event['title'] = format_timedelta(working_hours[date])

            if events_num % 2 != 0 and date != current_date:
                event['backgroundColor'] = 'red'

            if request.user.corrections.filter(time__date=date).exists():
                event['backgroundColor'] = 'orange'

            calendar_events.append(event)

        return JsonResponse(calendar_events, safe=False)



    def _parse_date_parameter(self, request, param_name):
        date_format = '%Y-%m-%d'
        try:
            param = request.GET[param_name]
            date = datetime.strptime(param, date_format)
        except KeyError:
            return None, 'You have to provide {} parameter'.format(param_name)
        except ValueError:
            return None, 'Parameter "{}" has not valid date format ({})'.format(param_name, date_format)

        return timezone.make_aware(date), None

    def put(self, request, public_key):
        device = Device.objects.filter(public_key=public_key, is_trusted=True).first()
        device.last_seen = timezone.now()
        device.save()

        box = Box(settings.PRIVATE_KEY, PublicKey(device.public_key.encode(), encoder=HexEncoder))
        if not device:
            empty_response = box.encrypt(''.encode())
            return HttpResponse(status=401, content=hexlify(empty_response))

        card_uid = box.decrypt(unhexlify(request.body)).decode()
        card, was_created = Card.objects.get_or_create(uid=card_uid)
        card.save()
        response = ''
        if was_created:
            response = 'Registered'
        elif card.user:
            events_num = Event.objects.filter(user=card.user, created_at__date=timezone.now().date()).count()
            if events_num % 2 == 0:
                response = 'Entry'
            else:
                response = 'Exit'
            Event.objects.create(card=card, device=device).save()

        encrypted_response = box.encrypt(response.encode())
        return HttpResponse(hexlify(encrypted_response))


class PublicKeyView(View):
    def get(self, request):
        return HttpResponse(content=settings.PUBLIC_KEY)


class DeviceView(View):
    def get(self, request, public_key):
        device, was_created = Device.objects.get_or_create(public_key=public_key)
        device.last_seen = timezone.now()
        device.save()
        box = Box(settings.PRIVATE_KEY, PublicKey(device.public_key.encode(), encoder=HexEncoder))
        empty_response = box.encrypt(''.encode())
        if not device.is_trusted:
            return HttpResponse(status=401, content=hexlify(empty_response))
        return HttpResponse(content=hexlify(empty_response))