from operator import attrgetter
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django_permanent.settings import FIELD
from ..models import Event, Device


@receiver(post_delete, sender=Event)
def hard_delete_event_linked_objects(sender, instance, **kwargs):
    model_fields = sender._meta.get_fields(include_parents=False)
    fk_fields = filter(attrgetter('is_relation'), model_fields)

    for field in fk_fields:
        try:
            object = getattr(instance, field.name)
            is_deleted = getattr(object, FIELD)
        except (AttributeError, field.rel.model.DoesNotExist):
            pass
        else:
            if is_deleted:
                filter_conditions = {field.name: object}
                has_references = sender.objects.filter(**filter_conditions).exists()

                if not has_references:
                    object.delete(force=True)  # Hard delete

@receiver(post_save, sender=Device)
def update_default_label(sender, instance, **kwargs):
    if not instance.label:
        instance.label = 'Device {}'.format(instance.id)
        instance.save()