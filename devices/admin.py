from django.contrib.admin import ModelAdmin, SimpleListFilter
from django.conf import settings
from .models import Device, Card, Event

class DeviceAdmin(ModelAdmin):
    list_display = ('label', '_public_key', 'is_trusted', 'first_seen', 'last_seen')
    list_filter = ('is_trusted', 'first_seen', 'last_seen')
    search_fields = ('label', 'fingerprint')
    empty_value_display = 'never'

    def has_add_permission(self, request):
        return False

    def _public_key(self, instance):
        server_public_key = ':'.join(settings.PUBLIC_KEY[i:i+2] for i in range(0, len(settings.PUBLIC_KEY), 2))
        device_public_key = ':'.join(instance.public_key[i:i+2] for i in range(0, len(instance.public_key), 2))
        return server_public_key + '\n' + device_public_key


class HasAssignedUser(SimpleListFilter):
    title = 'has assigned users'
    parameter_name = 'has_user'

    OPTIONS = {'yes': False, 'no': True}

    def lookups(self, request, model_admin):
        return ((o, o.title()) for o in self.OPTIONS)

    def queryset(self, request, queryset):
        value = self.OPTIONS.get(self.value(), None)
        if value is not None:
            return queryset.filter(user__isnull=value)
        return queryset


class CardAdmin(ModelAdmin):
    list_display = ('__str__', 'user', 'last_used')
    list_filter = (HasAssignedUser, )
    search_fields = ('label', 'user__email')

    def has_add_permission(self, request):
        return False

    def last_used(self, card):
        return card.events.latest('created_at').created_at


class EventAdmin(ModelAdmin):
    list_display = ('id', 'user', 'device', 'card', 'created_at')
    readonly_fields = list_display
    search_fields = ('user', 'device', 'card')
    list_filter = ('created_at', )
    ordering = ('-created_at', )
    empty_value_display = 'system'


    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False