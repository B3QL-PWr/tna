from django.conf.urls import url
from .views import EventView, PublicKeyView, DeviceView

urlpatterns = [
    url(r'^events/(?P<public_key>[a-f0-9]{64})$', EventView.as_view(http_method_names=['put'])),
    url(r'^events$', EventView.as_view(http_method_names=['get']), name='events_calendar'),
    url(r'^public_key$', PublicKeyView.as_view()),
    url(r'^devices/(?P<public_key>[a-f0-9]{64})$', DeviceView.as_view()),
]