from datetime import timedelta
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django_permanent.query import PermanentQuerySet
from accounts.models import PermanentUser
from config.models import SemiPermanentModel


class Device(SemiPermanentModel):
    label = models.CharField(max_length=80, blank=True)
    public_key = models.TextField(editable=False)
    is_trusted = models.BooleanField(default=False)
    first_seen = models.DateTimeField(editable=False, auto_now_add=True)
    last_seen = models.DateTimeField(editable=False, null=True)

    def __str__(self):
        return self.label


class PermanentDevice(Device):
    objects = PermanentQuerySet.as_manager()

    class Meta:
        proxy = True


class Card(SemiPermanentModel):
    uid = models.CharField(max_length=64, editable=False)
    label = models.CharField(max_length=80, blank=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.SET_NULL, null=True, related_name='cards', blank=True)

    def __str__(self):
        label = self.label or 'Card {}'.format(self.id)
        return '{} ({})'.format(label, self.uid)


class PermanentCard(Card):
    objects = PermanentQuerySet.as_manager()

    class Meta:
        proxy = True


class EventQuerySet(models.QuerySet):

    def working_ranges(self):
        """ Split dates into time ranges for easier delta time calculation."""
        iterator = iter(self.values_list('created_at', flat=True))
        skip_element = False
        while True:
            if not skip_element:
                first = next(iterator)
            skip_element = False

            try:
                second = next(iterator)
            except StopIteration:
                second = timezone.now()
                if first.date() != second.date():
                    raise

            if first.date() == second.date():
                yield (first, second)
            else:
                first = second
                skip_element = True

    def worked_hours(self):
        return sum((end - start for start, end in self.working_ranges()), timedelta())

    def between(self, start_date, end_date):
        return self.filter(created_at__range=(start_date, end_date))


class EventManager(models.Manager):

    def last(self, datetime=None):
        if datetime:
            return self.filter(created_at__date=datetime).last()
        return super(EventManager, self).last()

    def between(self, *args, **kwargs):
        return self.get_queryset().between(*args, **kwargs)

    def get_queryset(self):
        return EventQuerySet(self.model, using=self._db)


class Event(models.Model):
    user = models.ForeignKey(PermanentUser, on_delete=models.DO_NOTHING, related_name='events', blank=True)
    device = models.ForeignKey(PermanentDevice, on_delete=models.DO_NOTHING, related_name='events', null=True)
    card = models.ForeignKey(PermanentCard, on_delete=models.DO_NOTHING, related_name='events', null=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    objects = EventManager()

    class Meta:
        ordering = ('created_at', )

    def __str__(self):
        return 'Event {}'.format(self.id)

    def save(self, *args, **kwargs):
        try:
            self.user
        except ObjectDoesNotExist:
            self.user = self.card.user
        super(Event, self).save(*args, **kwargs)