"""tna URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django import http
from django.conf import settings
from django.conf.urls import url, include
from django.views.generic import RedirectView

from .admin import admin_site


class RedirectStaffOnly(RedirectView):
    permanent = True
    pattern_name = 'login'
    query_string = True

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and not user.is_staff:
            self.url = '/'
            self.query_string = False
        return super(RedirectStaffOnly, self).get(request, *args, **kwargs)


urlpatterns = [
    url(r'^admin/login', RedirectStaffOnly.as_view()),
    url(r'^admin/logout', RedirectView.as_view(pattern_name='logout', permanent=True, query_string=True)),
    url(r'^admin/', admin_site.urls),
    url(r'^auth/', include('accounts.urls')),
    url(r'^dashboard/', include('dashboards.urls')),
    url(r'^api/', include('devices.urls')),
    url(r'^correct/', include('corrections.urls')),
    url(r'^$', RedirectView.as_view(pattern_name='dashboard')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]