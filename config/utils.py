def format_timedelta(td):
    minutes, seconds = divmod(td.seconds + td.days * 86400, 60)
    hours, minutes = divmod(minutes, 60)
    return '{:02d}:{:02d}'.format(hours, minutes, seconds)

def disable_actions(app_admin):
    def get_actions(self, request):
        pass

    app_admin.get_actions = get_actions
    return app_admin