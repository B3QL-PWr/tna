"""
For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
from nacl.public import PrivateKey
from nacl.encoding import HexEncoder

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
def parent_dir(path, level=0):
    if level:
        path = os.path.dirname(path)
        return parent_dir(path, level-1)
    return path


BASE_DIR = parent_dir(os.path.abspath(__file__), 3)
# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',
]

THIRD_PARTY_APPS = [
    'bootstrap3',
    'django_permanent',
    'django_extensions'
]

LOCAL_APPS = [
    'accounts',
    'dashboards',
    'devices',
    'corrections'
]

INSTALLED_APPS = LOCAL_APPS + DJANGO_APPS + THIRD_PARTY_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

AUTH_USER_MODEL = 'accounts.User'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

BOOTSTRAP3 = {
    'jquery_url': STATIC_URL + 'jquery.min.js',
    'base_url': STATIC_URL + 'bootstrap/',
}

PASSWORD_RESET_TIMEOUT_DAYS = 1

SITE_ID = 1

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/auth/login'


def load_private_key(filename):
    try:
        with open(filename) as f:
            return PrivateKey(f.read(), encoder=HexEncoder)
    except FileNotFoundError:
        private_key = PrivateKey.generate()
        with open(filename, 'w') as f:
            f.write(private_key.encode(HexEncoder).decode())
        return private_key

PRIVATE_KEY = load_private_key(os.path.join(BASE_DIR, 'private_key.txt'))
PUBLIC_KEY = PRIVATE_KEY.public_key.encode(HexEncoder).decode()