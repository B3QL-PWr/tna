"""
For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

from . import *

SECRET_KEY = 'eewkp_vqo((z45rr3q8-#7$f7ou#dl9l)5mzrzgd5%j!+0*xsb'

DEBUG = True

INSTALLED_APPS += [
    'debug_toolbar'
]

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = ['127.0.0.1']
ALLOWED_HOSTS = INTERNAL_IPS + ['10.42.0.1']
