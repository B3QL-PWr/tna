from django_permanent.models import PermanentModel


class SemiPermanentModel(PermanentModel):

    class Meta:
        abstract = True

    def delete(self, using=None, force=False):
        if not force:
            force = not self.events.exists()
        super(SemiPermanentModel, self).delete(using, force)