from django.contrib.admin import AdminSite as DjangoAdminSite
from django.contrib.sites.admin import SiteAdmin, Site
from accounts.admin import UserAdmin, User
from devices.admin import Device, DeviceAdmin, Card, CardAdmin, Event, EventAdmin
from corrections.admin import ChangeRequest, ChangeRequestAdmin
from .utils import disable_actions


class AdminSite(DjangoAdminSite):
    site_header = 'Time&Attendance Administration'


admin_site = AdminSite()
admin_site.register(Site, disable_actions(SiteAdmin))
admin_site.register(User, disable_actions(UserAdmin))
admin_site.register(Device, disable_actions(DeviceAdmin))
admin_site.register(Card, disable_actions(CardAdmin))
admin_site.register(Event, disable_actions(EventAdmin))
admin_site.register(ChangeRequest, disable_actions(ChangeRequestAdmin))