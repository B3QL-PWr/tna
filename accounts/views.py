from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.views import PasswordChangeView as DjangoPasswordChangeView


class PasswordChangeView(SuccessMessageMixin, DjangoPasswordChangeView):
    success_message = 'Password changed successfully.'