from django import forms
from django.contrib.auth.forms import UserChangeForm as DjangoUserChangeForm
from .models import Groups


class UserCreationForm(forms.ModelForm):
    group = forms.ChoiceField(choices=Groups.items(), initial=Groups.USER.name)

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.group = self.cleaned_data['group']
        if commit:
            user.save()
        return user


class UserChangeForm(UserCreationForm, DjangoUserChangeForm):

    def __init__(self, *args, **kwargs):
        super(DjangoUserChangeForm, self).__init__(*args, **kwargs)
        self.fields['group'].initial = self.instance.group.name
