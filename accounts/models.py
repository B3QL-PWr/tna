from enum import Enum
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.db.models.manager import Manager
from django.utils.translation import ugettext_lazy as _
from django_permanent.query import NonDeletedQuerySet, PermanentQuerySet
from config.models import SemiPermanentModel

NonDeletedManager = Manager.from_queryset(NonDeletedQuerySet)


class UserManager(BaseUserManager, NonDeletedManager):
    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Groups(Enum):
    ADMIN = _('Administrator')
    MANAGER = _('Manager')
    USER = _('User')


    @classmethod
    def group_permissions(cls, group):
        permissions = {'is_superuser': False, 'is_staff': False}
        if group in (cls.ADMIN.name, cls.ADMIN.value):
            permissions['is_superuser'] = True
            permissions['is_staff'] = True
        if group in (cls.MANAGER.name, cls.MANAGER.value):
            permissions['is_staff'] = True
        return permissions

    @classmethod
    def user_group(cls, user):
        if user.is_superuser:
            return cls.ADMIN
        if user.is_staff:
            return cls.MANAGER
        return cls.USER

    @classmethod
    def items(cls):
        return ((group.name, group.value) for group in cls)


class User(SemiPermanentModel, AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    supervisor = models.ForeignKey('self', null=True, on_delete=models.PROTECT, limit_choices_to={'is_staff': True})
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def group(self):
        return Groups.user_group(self)

    @group.setter
    def group(self, value):
        permissions = Groups.group_permissions(value)
        for field_name, field_value in permissions.items():
            setattr(self, field_name, field_value)

    def has_perm(self, perm, obj=None):
        if self.group == Groups.MANAGER:
            return perm.startswith('corrections.')
        else:
            return super(User, self).has_perm(perm, obj)

class PermanentUser(User):
    objects = PermanentQuerySet.as_manager()

    class Meta:
        proxy = True