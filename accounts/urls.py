from django.conf.urls import url
from django.views.generic import RedirectView
from django.contrib.auth import views
from .views import PasswordChangeView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='login')),
    url(r'^login/$', views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(next_page='/'), name='logout'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.PasswordResetConfirmView.as_view(post_reset_login=True, success_url='/'), name='password_reset_confirm'),
    url(r'^password_reset/$', views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^password_change/$', PasswordChangeView.as_view(success_url='/'), name='password_change'),
]