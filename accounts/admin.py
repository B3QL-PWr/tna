from django.contrib.admin import SimpleListFilter
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .forms import UserCreationForm, UserChangeForm
from .models import Groups, User


class UsersGroupFilter(SimpleListFilter):
    title = 'Groups'
    parameter_name = 'group'

    def lookups(self, request, model_admin):
        return Groups.items()

    def queryset(self, request, queryset):
        group = self.value()
        if group:
            return queryset.filter(**Groups.group_permissions(group))
        return queryset.all()


class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password', 'group', 'supervisor')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'email', 'group', 'supervisor'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'group')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    add_form = UserCreationForm
    form = UserChangeForm
    list_filter = (UsersGroupFilter,)

    def group(self, obj):
        return obj.group.value