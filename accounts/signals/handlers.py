from django.contrib.auth.tokens import default_token_generator
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site

from ..models import User


@receiver(post_save, sender=User)
def send_user_password(sender, instance, created, raw, using, update_fields, **kwargs):
    if created and not instance.password:
        uidb64 = urlsafe_base64_encode(force_bytes(instance.pk))
        token = default_token_generator.make_token(instance)

        context = {
            'uidb64': uidb64,
            'token': token,
            'domain': get_current_site(request=None).domain
        }
        message = render_to_string('registration/new_account_email.txt', context=context)
        instance.email_user('Set new password', message, fail_silently=True)
