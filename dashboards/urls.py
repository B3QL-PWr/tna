from django.conf.urls import url
from .views import UserDashboard

urlpatterns = [
    url(r'^(?P<date>\d{4}-\d{2}-\d{2})$', UserDashboard.as_view(), name='dashboard_day'),
    url(r'^$', UserDashboard.as_view(), name='dashboard'),
]
