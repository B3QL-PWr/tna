$(document).ready(function() {
    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap3',
        firstDay: 1,
        aspectRatio: 2.5,
        defaultDate: CURRENT_DATE,
        header: {
          right: '',
        },
        footer: {
            right: 'prev,next',
        },
        events: EVENTS_ENDPOINT,
    })

});