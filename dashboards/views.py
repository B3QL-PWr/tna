from datetime import datetime, timedelta
from calendar import Calendar
from django.utils import timezone
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from config.utils import format_timedelta

class UserDashboard(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard.html'

    def get(self, request, *args, **kwargs):
        date = current_date = timezone.now().date()
        try:
            date = timezone.make_aware(datetime.strptime(kwargs['date'], '%Y-%m-%d'))
            date = date.date()
        except (TypeError, ValueError, KeyError):
            pass
        else:
            kwargs['events'] = request.user.events.filter(created_at__date=date).order_by('-created_at')
            kwargs['missing_event'] = date != current_date and len(kwargs['events']) % 2 != 0
            kwargs['correction'] = request.user.corrections.filter(time__date=date).first()
        kwargs['date'] = date.isoformat()
        kwargs['summary'] = self._get_summary(request.user, date)
        return super(UserDashboard, self).get(request, *args, **kwargs)

    def _get_summary(self, user, date):
        month = date.month
        weeks = Calendar().monthdatescalendar(date.year, date.month)
        summary = []
        total_hours = timedelta()
        for num, week in enumerate(weeks):
            week = list(datetime(d.year, d.month, d.day) for d in  week if d.month == month)
            first_day = timezone.make_aware(week[0])
            last_day = timezone.make_aware(week[-1])
            if first_day == last_day:
                worked_hours = user.events.filter(created_at__date=first_day).worked_hours()
            else:
                worked_hours = user.events.between(first_day, last_day).worked_hours()
            total_hours += worked_hours
            summary.append(format_timedelta(worked_hours))
        return summary + [format_timedelta(total_hours)]